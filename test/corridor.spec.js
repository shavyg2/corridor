var c = require("../");

var should = require("chai").should();

describe('Corridor', () => {

  it('should create object', done => {
    var object = c("shav/dev");
    should.exist(object);

    object.name="test";
    done()
  });


  it("should get object",function(){
    c("shav/dev").name.should.equal("test");
  })
});
