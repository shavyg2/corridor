var aisle = require("aisle");

module.exports = function(namespace){
  return aisle(namespace) || aisle(namespace,{}) || aisle(namespace);
}
